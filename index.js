// tambah
function tambah(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    return console.log("Harus masukan angka");
  }
  console.log(x + y);
}

// tambah(5, 6);

// Kurang
function kurang(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    return console.log("Harus masukan angka");
  }
  console.log(x - y);
}

// kurang(5, 3);

// kali
function kali(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    return console.log("Harus masukan angka");
  }
  console.log(x * y);
}

// bagi
function bagi(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    return console.log("Harus masukan angka");
  }
  console.log(x / y);
}

console.log("branch baru");
